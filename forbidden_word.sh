#!/bin/sh

if [ -z  "$1" ]
	then
	echo "No argument supplied"
fi

function Utest(){
	if [ -z  "$1" ]
	then
		printf "."
	else
		printf "*"
	fi
}

T01=$(grep -wi "while" $1)
T02=$(grep -wi "for" $1)
T03=$(grep -wi "redo" $1)
T04=$(grep -wi "break" $1)
T05=$(grep -wi "retry" $1)
T06=$(grep -wi "until" $1)

Utest "${T01}";
Utest "${T02}";
Utest "${T03}";
Utest "${T04}";
Utest "${T05}";
Utest "${T06}";

echo " :: END"

