#!/usr/bin/env ruby -w

#==============================================================================#
#------------------------------------------------------------------------------#
class Elem
	attr_reader :tag, :content, :tag_type, :opt, :tag_contain

	def initialize(content = [], opt = {})
		@tag = ""
		@tag_type = ""
		@tag_contain = "no"
		@content = content
		@opt = opt
	end

	def add_content(*new_content)
		new_content.each { |e|
			@content << e
		}
	end

	def to_s
		finish = false
		s = '<' + self.tag
		@opt.each{ |key, value|
			s = s + " #{key}='#{value}'"
		}
		if @tag_type.eql?("double")
			if @tag_contain.eql?("yes")
				s = s + ">\n"
			else
				s = s + ">"
			end
			finish = true
		end
		if @content.class == [].class
			@content.each{|e| s = s + e.to_s + "\n"}
		else
			s += @content.to_s
		end
		if finish == true
			s = s + "</#{@tag}>"
		elsif @tag_type.eql?("unclosed")
			s = s + ">"
		else
			s = s + " />"
		end
	end
end

#==============================================================================#
#------------------------------------------------------------------------------#

class Double < Elem
	attr_reader :tag_type
	def initialize(content = [], opt = {})
		super
		@tag_type = "double"
	end
end

class Simple < Elem
	attr_reader :tag_type
	def initialize(content = [], opt = {})
		super
		@tag_type = "simple"
	end
end

class Unclosed < Elem

	attr_reader :tag_type
	def initialize(content = [], opt = {})
		super
		@tag_type = "unclosed"
	end
end

#==============================================================================#
#------------------------------------------------------------------------------#

class Html < Double
	attr_reader :tag, :tag_contain
	def initialize(content = [], opt = {})
		super
		@tag = "Html"
		@tag_contain = "yes"
	end
end

class Head< Double
	attr_reader :tag, :tag_contain
	def initialize(content = [], opt = {})
		super
		@tag = "Head"
		@tag_contain = "yes"
	end
end

class Body < Double
	attr_reader :tag, :tag_contain
	def initialize(content = [], opt = {})
		super
		@tag = "Body"
		@tag_contain = "yes"
	end
end

class Title < Double
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "Title"
	end
end

class Meta < Unclosed
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "Meta"
	end
end

class Img < Simple
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "Img"
	end
end

class Table < Double
	attr_reader :tag, :tag_contain
	def initialize(content = [], opt = {})
		super
		@tag = "Table"
		@tag_contain = "yes"
	end
end

class Tr < Double
	attr_reader :tag, :tag_contain
	def initialize(content = [], opt = {})
		super
		@tag = "Tr"
		@tag_contain = "yes"
	end
end

class Th < Double
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "Th"
	end
end

class Td < Double
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "Td"
	end
end

class Ul < Double
	attr_reader :tag, :tag_contain
	def initialize(content = [], opt = {})
		super
		@tag = "Ul"
		@tag_contain = "yes"
	end
end

class Ol < Double
	attr_reader :tag, :tag_contain
	def initialize(content = [], opt = {})
		super
		@tag = "Ol"
		@tag_contain = "yes"
	end
end

class Li < Double
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "Li"
	end
end

class H1 < Double
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "H1"
	end
end

class H2 < Double
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "H2"
	end
end

class P < Double
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "P"
	end
end

class Div < Double
	attr_reader :tag, :tag_contain
	def initialize(content = [], opt = {})
		super
		@tag = "Div"
		@tag_contain = "yes"
	end
end

class Span < Double
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "Span"
	end
end

class Hr < Unclosed
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "Hr"
	end
end

class Br < Unclosed
	attr_reader :tag
	def initialize(content = [], opt = {})
		super
		@tag = "Br"
	end
end


#==============================================================================#
#------------------------------------------------------------------------------#

if $PROGRAM_NAME == __FILE__
	a = Html.new("bite")
	b = Head.new
	c = Body.new
	d = Title.new
	e = Meta.new
	f = Img.new
	g = Table.new

	puts a, b, c, d, e, f , g
	puts "-------"
	puts Html.new([
		Head.new([Title.new("Hello ground!")]),
		Body.new([H1.new("Oh no, not again!"),
		Img.new([], {'src':'imgur ma gueule'})
	])
	])
end
