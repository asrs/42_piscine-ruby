#!/usr/bin/env ruby -w

def		explain(str)
	states = {
		"Oregon"		=> "OR",
		"Alabama"		=> "AL",
		"New Jersey"	=> "NJ",
		"Colorado"		=> "CO"
	}
	
	capitals_cities= {
		"OR"		=> "Salem",
		"AL"		=> "Montgomery",
		"NJ"		=> "Trenton",
		"CO"		=> "Denver"
	}

	if capitals_cities.has_value?(str)
		state_now = states.invert[capitals_cities.invert[str]]
		akr_now = states[state_now]
		puts "#{str} is the capital of #{state_now} (akr: #{akr_now})"
	elsif states.invert.has_value?(str)
		cities_now = capitals_cities[states[str]]
		akr_now = states[str]
		puts "#{cities_now} is the capital of #{str} (akr: #{akr_now})"
	elsif str.empty?
		return
	else
		puts "#{str} is neither a capital city nor a state"
	end
end

def		main

	if ARGV.count != 1 || ARGV[0].class != "".class
		return
	end
	tab = ARGV[0].split(",").map{|el| el.strip.capitalize};
	tab.each{|e| explain(e)}
end

main
