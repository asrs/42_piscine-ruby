#!/usr/bin/env ruby -w

def		where
	states = {
		"Oregon"		=> "OR",
		"Alabama"		=> "AL",
		"New Jersey"	=> "NJ",
		"Colorado"		=> "CO"
	}
	
	capitals_cities= {
		"OR"		=> "Salem",
		"AL"		=> "Montgomery",
		"NJ"		=> "Trenton",
		"CO"		=> "Denver"
	}

	if ARGV.count != 1
		return
	elsif states[ARGV[0]].nil? == true
		puts "Unknown state"
	else
		puts capitals_cities[states[ARGV[0]]]
	end
end

where
