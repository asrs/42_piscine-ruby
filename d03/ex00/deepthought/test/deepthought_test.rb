require "test_helper"

class DeepthoughtTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Deepthought::VERSION
  end
end

def test_pure()
	puts "ENTER"
	deep = Deepthought.new
	assert_kind_of(Deepthought, deep, "Should be an instance of Deepthought")
	assert_equal(deep.respond("chat?"), "flip" )
end
